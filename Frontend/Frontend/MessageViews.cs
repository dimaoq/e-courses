﻿using System.Windows.Forms;
using System;

namespace Frontend
{
    interface IMessageView
    {
        void ShowErrorMessage(params string[] args);
        void ShowWarningMessage(params string[] args);
        void ShowInfoMessage(params string[] args);
    }

    class MessageBoxView : IMessageView
    {
        public void ShowErrorMessage(params string[] args)
        {
            MessageBox.Show("Ошибка: " + Environment.NewLine + string.Join("", args), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowWarningMessage(params string[] args)
        {
            MessageBox.Show(string.Join("", args), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public void ShowInfoMessage(params string[] args)
        {
            MessageBox.Show(string.Join("", args), "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
