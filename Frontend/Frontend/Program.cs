﻿using System;
using System.Windows.Forms;

namespace Frontend
{
    static class Program
    {
        static private string TCPIP_HOST = "localhost";
        static private int TCPIP_PORT = 7548;

        private const string LOAD_FROM_INI_ARGUMENT = "-ini";
        private const string CONFIG_INI_FILENAME = "settings.ini";

        static private void ConfigureTCPIP(string[] args)
        {
            if ((args.Length > 0) && (args[0] == LOAD_FROM_INI_ARGUMENT))
            {
                IniFile iniFile = new IniFile(CONFIG_INI_FILENAME);
                if (!iniFile.Exists())
                {
                    MessageBox.Show("Ini file not found: " + CONFIG_INI_FILENAME, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    const string SERVER_SECTION = "server";
                    string host = iniFile.ReadValue(SERVER_SECTION, "host");
                    string port = iniFile.ReadValue(SERVER_SECTION, "port");

                    if (host.Length > 0)
                    {
                        TCPIP_HOST = host;
                    }
                    if (port.Length > 0)
                    {
                        int newPort = 0;
                        if (int.TryParse(port, out newPort))
                        {
                            TCPIP_PORT = newPort;
                        }
                    }
                }
            }

            TCPGlobalInstances.InitTCPClientController(TCPIP_HOST, TCPIP_PORT);
        }

        [STAThread]
        static void Main(string[] args)
        {
            ConfigureTCPIP(args);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmStartScreen());
        }
    }
}
