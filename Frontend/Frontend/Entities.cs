﻿using System;
using System.Collections.Generic;

namespace Frontend
{
    static class Entities
    {
        public class AvailableCourse
        {
            public int Id;
            public string Name;
            public string Description;
            public string ReleaseDate;
            public bool IsEnrolledByUser;
        }

        public class CourseDetails
        {
            public string Name;
            public string Author;
            public string ReleaseDate;
            public int NumberOfMaterials;
            public string Description;
        }

        public class EnrolledCourses
        {
            public int Id;
            public string Name;
            public string ReleaseDate;
        }

        public class ManagedCourses
        {
            public int Id;
            public string Name;
        }

        public class EndedCourse
        {
            public int Id;
            public string Name;
            public string Description;
        }

        public class UserInfo
        {
            public string Name;
            public string Surname;
            public List<EndedCourse> EndedCourses;
        }

        public class Material
        {
            public int ID;
            public DateTime ReleaseDate;
            public string Description;
            public string Video;
        }

        public class Course
        {
            public string Name;
            public string Description;
            public List<Material> Materials;
            public bool IsEnded;
        }
    }
}
