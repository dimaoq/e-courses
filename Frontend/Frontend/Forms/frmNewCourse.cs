﻿using System;
using System.Windows.Forms;

namespace Frontend
{
    public partial class frmNewCourse : Form
    {
        private IMessageView msgView = new MessageBoxView();

        private bool CheckTextBox(TextBox txt, string descr)
        {
            if (txt.TextLength == 0)
            {
                MessageBox.Show(descr + " не может быть пустым", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        public frmNewCourse()
        {
            InitializeComponent();
        }

        private void frmNewCourse_Load(object sender, EventArgs e)
        {
            var now = DateTime.Now;
            releasePicker.MinDate = now;
            releasePicker.MaxDate = now.AddMonths(6); 
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!CheckTextBox(txtCourseName, "Название курса") || !CheckTextBox(txtDescription, "Описание курса"))
            {
                return;
            }

            string courseName = txtCourseName.Text;
            string description = txtDescription.Text;
            string releaseDate = releasePicker.Value.ToString("yyyy-MM-dd");
            int numberOfMaterials = (int)numMaterials.Value;

            var response = TCPGlobalInstances.GetTCPServerQueryController().AddNewCourse(courseName, description, releaseDate, numberOfMaterials);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage("Курс не был создан - ", response.error);
            }
            else
            {
                msgView.ShowInfoMessage("Курс создан!");
            }

            this.Close();
        }
    }
}
