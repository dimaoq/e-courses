﻿using System;
using System.Windows.Forms;
using System.Web.Script.Serialization;

namespace Frontend
{
    public partial class frmCourseMaterials : Form
    {
        private IMessageView msgView = new MessageBoxView();

        private int mCourseID;

        public frmCourseMaterials(int courseID)
        {
            mCourseID = courseID;
            InitializeComponent();
        }

        private void ShowMaterial(string materialName, int materialID)
        {
            frmMaterial frmMat = new frmMaterial(materialName, materialID);
            frmMat.ShowDialog();
        }

        private void ClearTableLayout(BufferedTableLayout panel)
        {
            for (int lastRow = panel.RowCount - 1; lastRow > 0; --lastRow)
            {
                panel.RowStyles.RemoveAt(lastRow);
                for (int column = 0; column < panel.ColumnCount; ++column)
                {
                    var control = panel.GetControlFromPosition(column, lastRow);
                    panel.Controls.Remove(control);
                }

                panel.RowCount--;
            }
        }

        private void ShowMaterials(Entities.Course course)
        {
            txtCourseName.Text = course.Name;
            txtDescription.Text = course.Description;

            tableLayoutMaterials.SuspendLayout();
            ClearTableLayout(tableLayoutMaterials);

            var now = DateTime.Now;
            foreach (var material in course.Materials)
            {
                if (material.ReleaseDate > now)
                {
                    break;
                }

                int prevRowCount = tableLayoutMaterials.RowCount;
                tableLayoutMaterials.RowCount++;
                tableLayoutMaterials.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                tableLayoutMaterials.Controls.Add(new TableLayoutLabel(material.ReleaseDate.ToShortDateString()), 0, prevRowCount);
                tableLayoutMaterials.Controls.Add(new TableLayoutLabel(material.Description), 1, prevRowCount);
                var btn = new TableLayoutButton(material.ID, TableLayoutButton.ButtonStyle.EnterCourse);
                btn.Click += (senderObj, eventArgs) => ShowMaterial(material.Description, material.ID);
                tableLayoutMaterials.Controls.Add(btn, 2, prevRowCount);
            }

            tableLayoutMaterials.ResumeLayout();
        }

        private void frmCourseMaterials_Load(object sender, EventArgs e)
        {
            tableLayoutMaterials.Width = this.ClientSize.Width;
            tableLayoutMaterials.SetMaximumHeight(-tableLayoutMaterials.Top + btnCancel.Top - 10);

            var response = TCPGlobalInstances.GetTCPServerQueryController().GetFullCourseInfo(mCourseID);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                var course = new JavaScriptSerializer().Deserialize<Entities.Course>(response.textQuery);
                ShowMaterials(course);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
