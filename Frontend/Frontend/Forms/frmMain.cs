﻿using System;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Drawing;

namespace Frontend
{
    public partial class frmMain : Form
    {
        private IMessageView msgView = new MessageBoxView();

        private string session;
        private bool isLogout;

        public frmMain(string newSession)
        {
            session = newSession;
            isLogout = false;
            InitializeComponent();
        }

        private void SetTableLayoutsSizes(BufferedTableLayout panel)
        {
            panel.Left = 0;
            panel.Top = 120;
            panel.Width = this.ClientSize.Width;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            SetTableLayoutsSizes(tableLayoutAvailable);
            SetTableLayoutsSizes(tableLayoutManageCourses);
            SetTableLayoutsSizes(tableLayoutStartedCourses);

            ResizeAllTableLayoutPanels();
        }

        private bool ResizeTableLayout(BufferedTableLayout panel)
        {
            if (panel.Visible)
            {   
                panel.SetMaximumHeight(this.ClientSize.Height - panel.Top);
                panel.Update();
                panel.ResumeLayout();
                return true;
            }

            return false;
        }

        private void ResizeAllTableLayoutPanels()
        {
            var expr = ResizeTableLayout(tableLayoutAvailable) ||
                       ResizeTableLayout(tableLayoutStartedCourses) ||
                       ResizeTableLayout(tableLayoutManageCourses);
        }

        private void ClearTableLayout(BufferedTableLayout panel)
        {
            for (int lastRow = panel.RowCount - 1; lastRow > 0; --lastRow)
            {
                panel.RowStyles.RemoveAt(lastRow);
                for (int column = 0; column < panel.ColumnCount; ++column)
                {
                    var control = panel.GetControlFromPosition(column, lastRow);
                    panel.Controls.Remove(control);
                }

                panel.RowCount--;
            }
        }

        private void DoEnroll(int courseID)
        {
            frmCourseDetails details = new frmCourseDetails(courseID);
            details.ShowDialog();
            ShowAvailableCourses();
        }

        private void DoUnenroll(int courseID)
        {
            var response = TCPGlobalInstances.GetTCPServerQueryController().DoUnenroll(courseID);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                msgView.ShowInfoMessage("Подписка на курс успешно отменена!");
                ShowAvailableCourses();
            }
        }

        private void OpenCourse(int courseID)
        {
            frmCourseMaterials course = new frmCourseMaterials(courseID);
            course.ShowDialog();
            ShowEnrolledCourses();
        }

        private void OpenManagedCourse(int courseID)
        {
            frmManageCourse manageCourse = new frmManageCourse(courseID);
            manageCourse.ShowDialog();
            ShowManagedCourses();
        }

        private void ShowAvailableCourses()
        {
            var response = TCPGlobalInstances.GetTCPServerQueryController().GetAvailableCourses();
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                var courses = new JavaScriptSerializer().Deserialize<List<Entities.AvailableCourse>>(response.textQuery);
                int i = 0;
                tableLayoutAvailable.SuspendLayout();

                ClearTableLayout(tableLayoutAvailable);
                foreach (var course in courses)
                {
                    int prevRowCount = tableLayoutAvailable.RowCount;
                    tableLayoutAvailable.RowCount++;
                    tableLayoutAvailable.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                    tableLayoutAvailable.Controls.Add(new TableLayoutLabel(course.Name), 0, prevRowCount);
                    tableLayoutAvailable.Controls.Add(new TableLayoutLabel(course.Description), 1, prevRowCount);
                    tableLayoutAvailable.Controls.Add(new TableLayoutLabel(course.ReleaseDate), 2, prevRowCount);
                    var btn = new TableLayoutButton(course.Id,
                                                   (course.IsEnrolledByUser ? TableLayoutButton.ButtonStyle.AlreadyEnrolled : TableLayoutButton.ButtonStyle.NotEnrolled));
                    btn.Click += (sender, e) => 
                    {
                        if (course.IsEnrolledByUser)
                        {
                            DoUnenroll(course.Id);
                        }
                        else
                        {
                            DoEnroll(course.Id);
                        }
                    };
                    tableLayoutAvailable.Controls.Add(btn, 3, prevRowCount);
                    i++;
                }

                ResizeAllTableLayoutPanels();
                tableLayoutAvailable.ResumeLayout();
            }
        }

        private void ShowEnrolledCourses()
        {
            var response = TCPGlobalInstances.GetTCPServerQueryController().GetEnrolledCourses();
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                var courses = new JavaScriptSerializer().Deserialize<List<Entities.EnrolledCourses>>(response.textQuery);
                tableLayoutStartedCourses.SuspendLayout();

                ClearTableLayout(tableLayoutStartedCourses);
                foreach (var course in courses)
                {
                    int prevRowCount = tableLayoutStartedCourses.RowCount;
                    tableLayoutStartedCourses.RowCount++;
                    tableLayoutStartedCourses.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                    tableLayoutStartedCourses.Controls.Add(new TableLayoutLabel(course.Name), 0, prevRowCount);
                    tableLayoutStartedCourses.Controls.Add(new TableLayoutLabel(course.ReleaseDate), 1, prevRowCount);
                    var btn = new TableLayoutButton(course.Id, TableLayoutButton.ButtonStyle.EnterCourse);
                    btn.Click += (sender, e) => OpenCourse(course.Id);
                    tableLayoutStartedCourses.Controls.Add(btn, 2, prevRowCount);
                }

                ResizeAllTableLayoutPanels();
                tableLayoutStartedCourses.ResumeLayout();
            }
        }

        private void ShowManagedCourses()
        {
            var response = TCPGlobalInstances.GetTCPServerQueryController().GetManagedCourses();
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                var courses = new JavaScriptSerializer().Deserialize<List<Entities.ManagedCourses>>(response.textQuery);
                int i = 0;
                tableLayoutManageCourses.SuspendLayout();

                ClearTableLayout(tableLayoutManageCourses);
                foreach (var course in courses)
                {
                    int prevRowCount = tableLayoutManageCourses.RowCount;
                    tableLayoutManageCourses.RowCount++;
                    tableLayoutManageCourses.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                    tableLayoutManageCourses.Controls.Add(new TableLayoutLabel(course.Name), 0, prevRowCount);
                    var btn = new TableLayoutButton(course.Id, TableLayoutButton.ButtonStyle.EnterCourse);
                    btn.Click += (sender, e) => OpenManagedCourse(course.Id);
                    tableLayoutManageCourses.Controls.Add(btn, 1, prevRowCount);
                    i++;
                }

                ResizeAllTableLayoutPanels();
                tableLayoutManageCourses.ResumeLayout();
            }
        }

        private void optAvailableCourses_CheckedChanged(object sender, EventArgs e)
        {
            if (optAvailableCourses.Checked)
            {
                tableLayoutAvailable.Visible = true;
                tableLayoutManageCourses.Visible = false;
                tableLayoutStartedCourses.Visible = false;
                btnNewCourse.Visible = false;
                ShowAvailableCourses();
            }
        }

        private void optStartedCourses_CheckedChanged(object sender, EventArgs e)
        {
            if (optStartedCourses.Checked)
            {
                tableLayoutAvailable.Visible = false;
                tableLayoutManageCourses.Visible = false;
                tableLayoutStartedCourses.Visible = true;
                btnNewCourse.Visible = false;
                ShowEnrolledCourses();
            }
        }

        private void optManageCourses_CheckedChanged(object sender, EventArgs e)
        {
            if (optManageCourses.Checked)
            {
                tableLayoutAvailable.Visible = false;
                tableLayoutManageCourses.Visible = true;
                tableLayoutStartedCourses.Visible = false;
                btnNewCourse.Visible = true;
                ShowManagedCourses();
            }
        }

        private void btnNewCourse_Click(object sender, EventArgs e)
        {
            frmNewCourse newCourse = new frmNewCourse();
            newCourse.ShowDialog();
            ShowManagedCourses();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            ResizeAllTableLayoutPanels();
        }

        private void btnMyProfile_Click(object sender, EventArgs e)
        {
            frmProfile profile = new frmProfile();
            profile.ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            isLogout = true;
            this.Close();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isLogout)
            {
                Application.Exit();
            }
        }
    }
}
