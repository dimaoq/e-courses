﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace Frontend
{
    public partial class frmStartScreen : Form
    {
        private IMessageView msgView = new MessageBoxView();

        public frmStartScreen()
        {
            InitializeComponent();
        }

        private bool CheckTextBox(TextBox txt, string descr)
        {
            if (txt.TextLength == 0)
            {
                MessageBox.Show(descr + " не может быть пустым", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        private void ClearTextBox(TextBox txt)
        {
            txt.Text = "";
        }

        private string createHash(string pass)
        {
            SHA256Managed crypt = new SHA256Managed();
            var bytes = Encoding.UTF8.GetBytes(pass);
            var hash = crypt.ComputeHash(bytes);
            string hashString = "";
            foreach (byte x in hash)
            {
                hashString += string.Format("{0:x2}", x);
            }

            return hashString;
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            string login = txtLogin.Text;
            string pass = txtPassword.Text;

            string hash = createHash(pass);
            hash = hash.Substring(0, 64);
            pass = "";

            var response = TCPServerQuery.GetSimpleResponse("getSession", "", login, hash);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                ClearTextBox(txtPassword);
                string session = response.textQuery;
                TCPGlobalInstances.InitTCPServerQuery(session);

                frmMain main = new frmMain(session);
                this.Visible = false;
                main.ShowDialog();
                this.Visible = true;
            }
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            if (!CheckTextBox(txtRegLogin, "Поле \"Логин\"") ||
                !CheckTextBox(txtRegPass, "Поле \"Описание\"") ||
                !CheckTextBox(txtName, "Поле \"Имя\"") ||
                !CheckTextBox(txtSurname, "Поле \"Фамилия\""))
            {
                return;
            }

            string login = txtRegLogin.Text;
            string pass = txtRegPass.Text;
            string name = txtName.Text;
            string surname = txtSurname.Text;

            string hash = createHash(pass);
            hash = hash.Substring(0, 64);
            pass = "";

            var response = TCPServerQuery.GetSimpleResponse("createUser", "", login, hash, name, surname);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                msgView.ShowInfoMessage("Пользователь успешно создан!");
                ClearTextBox(txtRegLogin);
                ClearTextBox(txtRegPass);
                ClearTextBox(txtName);
                ClearTextBox(txtSurname);
            }
        }
    }
}
