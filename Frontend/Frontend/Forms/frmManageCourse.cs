﻿using System;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Drawing;

namespace Frontend
{
    public partial class frmManageCourse : Form
    {
        private IMessageView msgView = new MessageBoxView();

        private int mCourseID;
        private List<int> mMaterials; 

        private bool CheckTextBox(TextBox txt, string descr)
        {
            if (txt.TextLength == 0)
            {
                MessageBox.Show(descr + " не может быть пустым", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        public frmManageCourse(int courseID)
        {
            mCourseID = courseID;
            mMaterials = new List<int>();
            InitializeComponent();
        }

        private void DeleteTableMaterial(int row)
        {
            tableLayoutMaterials.RowStyles.RemoveAt(row);

            for (int columnIndex = 0; columnIndex < tableLayoutMaterials.ColumnCount; columnIndex++)
            {
                var control = tableLayoutMaterials.GetControlFromPosition(columnIndex, row);
                if (control != null)
                {
                    tableLayoutMaterials.Controls.Remove(control);
                }
            }

            for (int i = row + 1; i < tableLayoutMaterials.RowCount; i++)
            {
                for (int columnIndex = 0; columnIndex < tableLayoutMaterials.ColumnCount; columnIndex++)
                {
                    var control = tableLayoutMaterials.GetControlFromPosition(columnIndex, i);
                    if (control != null)
                    {
                        tableLayoutMaterials.SetRow(control, i - 1);
                    }
                }
            }

            tableLayoutMaterials.RowCount--;
        }

        private void DeleteMaterial(int row)
        {
            DeleteTableMaterial(row);
            int materialID = mMaterials[row - 1];
            mMaterials.RemoveAt(row - 1);
            if (materialID != -1)
            {
                var response = TCPGlobalInstances.GetTCPServerQueryController().DeleteMaterial(materialID);
                if (response.error.Length > 0)
                {
                    msgView.ShowErrorMessage(response.error);
                }
                else
                {
                    msgView.ShowInfoMessage("Материал успешно удален!");
                }
            }
        }

        private void SwapControls(int column, int rowUp, int rowDown)
        {
            var controlUp = tableLayoutMaterials.GetControlFromPosition(column, rowUp);
            var controlDown = tableLayoutMaterials.GetControlFromPosition(column, rowDown);

            tableLayoutMaterials.Controls.Remove(controlUp);
            tableLayoutMaterials.Controls.Remove(controlDown);
            tableLayoutMaterials.Controls.Add(controlDown, column, rowUp);
            tableLayoutMaterials.Controls.Add(controlUp, column, rowDown);
        }

        private void SwapValues(int column, int rowUp, int rowDown)
        {
            var controlUp = tableLayoutMaterials.GetControlFromPosition(column, rowUp);
            var controlDown = tableLayoutMaterials.GetControlFromPosition(column, rowDown);

            var tempValue = controlUp.Text;
            controlUp.Text = controlDown.Text;
            controlDown.Text = tempValue;
        }

        private void MoveRows(int rowUp, int rowDown)
        {
            int upMaterialID = mMaterials[rowUp - 1];
            mMaterials[rowUp - 1] = mMaterials[rowDown - 1];
            mMaterials[rowDown - 1] = upMaterialID;
               
            tableLayoutMaterials.SuspendLayout();

            SwapControls(0, rowUp, rowDown);
            SwapValues(1, rowUp, rowDown);
            SwapValues(2, rowUp, rowDown);
            SwapValues(3, rowUp, rowDown);
            SwapControls(4, rowUp, rowDown);

            tableLayoutMaterials.ResumeLayout();
        }

        private void MoveRowUp(int row)
        {
            if (row > 1)
            {
                MoveRows(row - 1, row);
            }
        }

        private void MoveRowDown(int row)
        {
            if (row < tableLayoutMaterials.RowCount - 1)
            {
                MoveRows(row, row + 1);
            }
        }

        private void AddMaterial(int materialID, DateTime releaseDate, string description, string video, bool isEnded)
        {
            int prevRowCount = tableLayoutMaterials.RowCount;
            tableLayoutMaterials.RowCount++;
            tableLayoutMaterials.RowStyles.Add(new RowStyle(SizeType.AutoSize));

            Panel panel = new Panel();
            panel.AutoSize = true;
            panel.Anchor = AnchorStyles.None;
            panel.Dock = DockStyle.Fill;

            var btnUp = new TableLayoutButton(materialID, TableLayoutButton.ButtonStyle.RowUp);
            btnUp.Left = 0;
            btnUp.Top = 0;
            btnUp.Click += (senderObj, eventArgs) => MoveRowUp(tableLayoutMaterials.GetCellPosition(panel).Row);

            var btnDown = new TableLayoutButton(materialID, TableLayoutButton.ButtonStyle.RowDown);
            btnDown.Left = 0;
            btnDown.Top = 25;
            btnDown.Click += (senderObj, eventArgs) => MoveRowDown(tableLayoutMaterials.GetCellPosition(panel).Row);

            panel.Controls.Add(btnUp);
            panel.Controls.Add(btnDown);

            var btn = new TableLayoutButton(materialID, TableLayoutButton.ButtonStyle.DeleteMaterial);
            btn.Click += (senderObj, eventArgs) => DeleteMaterial(tableLayoutMaterials.GetCellPosition(btn).Row);

            if (isEnded)
            {
                panel.Enabled = false;
                btn.Enabled = false;
            }

            tableLayoutMaterials.Controls.Add(panel, 0, prevRowCount);
            tableLayoutMaterials.Controls.Add(new TableLayoutDateTimePicker(releaseDate), 1, prevRowCount);
            tableLayoutMaterials.Controls.Add(new TableLayoutTextBox(description), 2, prevRowCount);
            tableLayoutMaterials.Controls.Add(new TableLayoutTextBox(video), 3, prevRowCount);
            tableLayoutMaterials.Controls.Add(btn, 4, prevRowCount);
        }

        private void frmManageCourse_Load(object sender, EventArgs e)
        {
            tableLayoutMaterials.Width = this.ClientSize.Width;

            var response = TCPGlobalInstances.GetTCPServerQueryController().GetFullCourseInfo(mCourseID);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                var course = new JavaScriptSerializer().Deserialize<Entities.Course>(response.textQuery);

                txtCourseName.Text = course.Name;
                txtDescription.Text = course.Description;

                if (course.IsEnded)
                {
                    btnAdd.Enabled = false;
                    btnEnd.Enabled = false;
                }

                tableLayoutMaterials.SuspendLayout();

                foreach (var material in course.Materials)
                {
                    mMaterials.Add(material.ID);
                    AddMaterial(material.ID, material.ReleaseDate, material.Description, material.Video, course.IsEnded);
                }

                tableLayoutMaterials.ResumeLayout();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!CheckTextBox(txtCourseName, "Поле \"Название курса\"") || 
                !CheckTextBox(txtDescription, "Поле \"Описание\""))
            {
                return;
            }

            var updateCourseResponse = TCPGlobalInstances.GetTCPServerQueryController().UpdateCourseTextInfo(mCourseID, txtCourseName.Text, txtDescription.Text);
            if (updateCourseResponse.error.Length > 0)
            {
                msgView.ShowErrorMessage(updateCourseResponse.error);
                return;
            }

            bool isOk = true;

            for (int i = 0; i < mMaterials.Count && isOk; ++i)
            {
                TableLayoutDateTimePicker releaseDateControl = (TableLayoutDateTimePicker)tableLayoutMaterials.GetControlFromPosition(1, i + 1);
                TableLayoutTextBox descriptionControl = (TableLayoutTextBox)tableLayoutMaterials.GetControlFromPosition(2, i + 1);
                TableLayoutTextBox videoControl = (TableLayoutTextBox)tableLayoutMaterials.GetControlFromPosition(3, i + 1);

                string releaseDate = releaseDateControl.Value.ToString("yyyy-MM-dd");
                string description = descriptionControl.Text;
                string video = videoControl.Text;
                if (description.Length == 0 && video.Length == 0)
                {
                    continue;
                }

                if (mMaterials[i] != -1)
                {
                    var updateMaterialResponse = TCPGlobalInstances.GetTCPServerQueryController().UpdateMaterial(mMaterials[i], releaseDate, description, video);
                    if (updateMaterialResponse.error.Length > 0)
                    {
                        msgView.ShowErrorMessage(updateMaterialResponse.error);
                        isOk = false;
                    }
                }
                else
                {
                    var createMaterialResponse = TCPGlobalInstances.GetTCPServerQueryController().CreateMaterial(mCourseID, releaseDate, description, video);
                    if (createMaterialResponse.error.Length > 0)
                    {
                        msgView.ShowErrorMessage(createMaterialResponse.error);
                        isOk = false;
                    }
                    else
                    {
                        int newMaterialID;
                        if (!int.TryParse(createMaterialResponse.textQuery, out newMaterialID))
                        {
                            msgView.ShowErrorMessage("Было возвращен некорректный ID для нового материала!");
                            isOk = false;
                        }
                        else
                        {
                            mMaterials[i] = newMaterialID;
                        }
                    }
                }
            }

            if (isOk)
            {
                var updateOrder = TCPGlobalInstances.GetTCPServerQueryController().UpdateMaterialsOrder(mMaterials.ToArray());
                if (updateOrder.error.Length > 0)
                {
                    msgView.ShowErrorMessage(updateOrder.error);
                    isOk = false;
                }
                else
                {
                    msgView.ShowInfoMessage("Данные курса успешно изменены!");
                    this.Close();
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            mMaterials.Add(-1);

            tableLayoutMaterials.SuspendLayout();

            DateTime res = DateTime.Now;
            if (tableLayoutMaterials.RowCount > 1)
            {
                TableLayoutDateTimePicker picker = (TableLayoutDateTimePicker)tableLayoutMaterials.GetControlFromPosition(1, tableLayoutMaterials.RowCount - 1);
                res = picker.Value;
            }
            AddMaterial(-1, res, "", "", false);

            tableLayoutMaterials.ResumeLayout();
        }

        private void frmManageCourse_Resize(object sender, EventArgs e)
        {
            tableLayoutMaterials.SetMaximumHeight(-tableLayoutMaterials.Top + btnCancel.Top - 10);
        }

        private void btnEnd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Завершить курс?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var endCourseResult = TCPGlobalInstances.GetTCPServerQueryController().EndCourse(mCourseID);
                if (endCourseResult.error.Length > 0)
                {
                    msgView.ShowErrorMessage(endCourseResult.error);
                }
                else
                {
                    msgView.ShowInfoMessage("Курс успешно завершен!");
                    this.Close();
                }
            }
        }
    }
}
