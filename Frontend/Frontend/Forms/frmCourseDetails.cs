﻿using System;
using System.Windows.Forms;
using System.Web.Script.Serialization;

namespace Frontend
{
    public partial class frmCourseDetails : Form
    {
        private IMessageView msgView = new MessageBoxView();
        private int mCourseID;

        public frmCourseDetails(int id)
        {
            mCourseID = id;
            InitializeComponent();
        }

        private void frmCourseDetails_Load(object sender, EventArgs e)
        {
            var response = TCPGlobalInstances.GetTCPServerQueryController().GetCourseDetails(mCourseID);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
                this.Close();
            }
            else
            {
                var course = new JavaScriptSerializer().Deserialize<Entities.CourseDetails>(response.textQuery);

                txtCourseName.Text = course.Name;
                txtAuthor.Text = course.Author;
                lblReleaseDate.Text = course.ReleaseDate;
                lblNumberOfMaterials.Text = course.NumberOfMaterials.ToString();
                txtDescription.Text = course.Description;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEnroll_Click(object sender, EventArgs e)
        {
            var response = TCPGlobalInstances.GetTCPServerQueryController().DoEnroll(mCourseID);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                msgView.ShowInfoMessage("Успешно записаны на курс!");
                this.Close();
            }
        }
    }
}
