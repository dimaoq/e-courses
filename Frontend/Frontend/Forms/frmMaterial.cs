﻿using System;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace Frontend
{
    public partial class frmMaterial : Form
    {
        private IMessageView msgView = new MessageBoxView();

        private string mName;
        private int mMaterialID;

        public frmMaterial(string name, int materialID)
        {
            mName = name;
            mMaterialID = materialID;
            InitializeComponent();
        }

        private void frmMaterial_Load(object sender, EventArgs e)
        {
            this.Text = mName;
            player.settings.autoStart = false;

            var response = TCPGlobalInstances.GetTCPServerQueryController().GetMaterial(mMaterialID);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                var material = new JavaScriptSerializer().Deserialize<Entities.Material>(response.textQuery);

                txtDescription.Text = material.Description;
                player.URL = material.Video;
            }

            player.Ctlcontrols.stop();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
