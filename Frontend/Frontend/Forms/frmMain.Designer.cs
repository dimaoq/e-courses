﻿namespace Frontend
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.optAvailableCourses = new System.Windows.Forms.RadioButton();
            this.optStartedCourses = new System.Windows.Forms.RadioButton();
            this.btnMyProfile = new System.Windows.Forms.Button();
            this.optManageCourses = new System.Windows.Forms.RadioButton();
            this.btnNewCourse = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.tableLayoutManageCourses = new Frontend.BufferedTableLayout();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tableLayoutStartedCourses = new Frontend.BufferedTableLayout();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutAvailable = new Frontend.BufferedTableLayout();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutManageCourses.SuspendLayout();
            this.tableLayoutStartedCourses.SuspendLayout();
            this.tableLayoutAvailable.SuspendLayout();
            this.SuspendLayout();
            // 
            // optAvailableCourses
            // 
            this.optAvailableCourses.Appearance = System.Windows.Forms.Appearance.Button;
            this.optAvailableCourses.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.optAvailableCourses.Location = new System.Drawing.Point(12, 12);
            this.optAvailableCourses.Name = "optAvailableCourses";
            this.optAvailableCourses.Size = new System.Drawing.Size(137, 62);
            this.optAvailableCourses.TabIndex = 3;
            this.optAvailableCourses.TabStop = true;
            this.optAvailableCourses.Text = "Доступные курсы";
            this.optAvailableCourses.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.optAvailableCourses.UseVisualStyleBackColor = true;
            this.optAvailableCourses.CheckedChanged += new System.EventHandler(this.optAvailableCourses_CheckedChanged);
            // 
            // optStartedCourses
            // 
            this.optStartedCourses.Appearance = System.Windows.Forms.Appearance.Button;
            this.optStartedCourses.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.optStartedCourses.Location = new System.Drawing.Point(148, 12);
            this.optStartedCourses.Name = "optStartedCourses";
            this.optStartedCourses.Size = new System.Drawing.Size(137, 62);
            this.optStartedCourses.TabIndex = 4;
            this.optStartedCourses.TabStop = true;
            this.optStartedCourses.Text = "Начавшиеся курсы";
            this.optStartedCourses.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.optStartedCourses.UseVisualStyleBackColor = true;
            this.optStartedCourses.CheckedChanged += new System.EventHandler(this.optStartedCourses_CheckedChanged);
            // 
            // btnMyProfile
            // 
            this.btnMyProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMyProfile.AutoSize = true;
            this.btnMyProfile.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnMyProfile.Location = new System.Drawing.Point(725, 12);
            this.btnMyProfile.Name = "btnMyProfile";
            this.btnMyProfile.Size = new System.Drawing.Size(127, 27);
            this.btnMyProfile.TabIndex = 5;
            this.btnMyProfile.Text = "Мой профиль";
            this.btnMyProfile.UseVisualStyleBackColor = true;
            this.btnMyProfile.Click += new System.EventHandler(this.btnMyProfile_Click);
            // 
            // optManageCourses
            // 
            this.optManageCourses.Appearance = System.Windows.Forms.Appearance.Button;
            this.optManageCourses.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.optManageCourses.Location = new System.Drawing.Point(284, 12);
            this.optManageCourses.Name = "optManageCourses";
            this.optManageCourses.Size = new System.Drawing.Size(137, 62);
            this.optManageCourses.TabIndex = 7;
            this.optManageCourses.TabStop = true;
            this.optManageCourses.Text = "Управление своими курсами";
            this.optManageCourses.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.optManageCourses.UseVisualStyleBackColor = true;
            this.optManageCourses.CheckedChanged += new System.EventHandler(this.optManageCourses_CheckedChanged);
            // 
            // btnNewCourse
            // 
            this.btnNewCourse.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnNewCourse.Location = new System.Drawing.Point(284, 80);
            this.btnNewCourse.Name = "btnNewCourse";
            this.btnNewCourse.Size = new System.Drawing.Size(137, 26);
            this.btnNewCourse.TabIndex = 10;
            this.btnNewCourse.Text = "Создать новый курс";
            this.btnNewCourse.UseVisualStyleBackColor = true;
            this.btnNewCourse.Visible = false;
            this.btnNewCourse.Click += new System.EventHandler(this.btnNewCourse_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.AutoSize = true;
            this.btnLogout.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnLogout.Location = new System.Drawing.Point(725, 47);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(127, 27);
            this.btnLogout.TabIndex = 11;
            this.btnLogout.Text = "Выйти из профиля";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // tableLayoutManageCourses
            // 
            this.tableLayoutManageCourses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutManageCourses.AutoScroll = true;
            this.tableLayoutManageCourses.AutoSize = true;
            this.tableLayoutManageCourses.BackColor = System.Drawing.Color.White;
            this.tableLayoutManageCourses.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutManageCourses.ColumnCount = 2;
            this.tableLayoutManageCourses.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutManageCourses.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutManageCourses.Controls.Add(this.label9, 0, 0);
            this.tableLayoutManageCourses.Controls.Add(this.label12, 1, 0);
            this.tableLayoutManageCourses.Location = new System.Drawing.Point(0, 189);
            this.tableLayoutManageCourses.Name = "tableLayoutManageCourses";
            this.tableLayoutManageCourses.RowCount = 1;
            this.tableLayoutManageCourses.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutManageCourses.Size = new System.Drawing.Size(852, 69);
            this.tableLayoutManageCourses.TabIndex = 9;
            this.tableLayoutManageCourses.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(334, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Название курса";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(780, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 17);
            this.label12.TabIndex = 3;
            this.label12.Text = "Действие";
            // 
            // tableLayoutStartedCourses
            // 
            this.tableLayoutStartedCourses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutStartedCourses.AutoScroll = true;
            this.tableLayoutStartedCourses.AutoSize = true;
            this.tableLayoutStartedCourses.BackColor = System.Drawing.Color.White;
            this.tableLayoutStartedCourses.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutStartedCourses.ColumnCount = 3;
            this.tableLayoutStartedCourses.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutStartedCourses.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutStartedCourses.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutStartedCourses.Controls.Add(this.label5, 0, 0);
            this.tableLayoutStartedCourses.Controls.Add(this.label6, 1, 0);
            this.tableLayoutStartedCourses.Controls.Add(this.label8, 2, 0);
            this.tableLayoutStartedCourses.Location = new System.Drawing.Point(0, 140);
            this.tableLayoutStartedCourses.Name = "tableLayoutStartedCourses";
            this.tableLayoutStartedCourses.RowCount = 1;
            this.tableLayoutStartedCourses.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutStartedCourses.Size = new System.Drawing.Size(852, 69);
            this.tableLayoutStartedCourses.TabIndex = 8;
            this.tableLayoutStartedCourses.Visible = false;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(285, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Название курса";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(682, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Дата выхода";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(780, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Действие";
            // 
            // tableLayoutAvailable
            // 
            this.tableLayoutAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutAvailable.AutoScroll = true;
            this.tableLayoutAvailable.AutoSize = true;
            this.tableLayoutAvailable.BackColor = System.Drawing.Color.White;
            this.tableLayoutAvailable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutAvailable.ColumnCount = 4;
            this.tableLayoutAvailable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutAvailable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutAvailable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutAvailable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutAvailable.Controls.Add(this.label1, 0, 0);
            this.tableLayoutAvailable.Controls.Add(this.label2, 1, 0);
            this.tableLayoutAvailable.Controls.Add(this.label3, 2, 0);
            this.tableLayoutAvailable.Controls.Add(this.label4, 3, 0);
            this.tableLayoutAvailable.Location = new System.Drawing.Point(0, 100);
            this.tableLayoutAvailable.Name = "tableLayoutAvailable";
            this.tableLayoutAvailable.RowCount = 1;
            this.tableLayoutAvailable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutAvailable.Size = new System.Drawing.Size(852, 78);
            this.tableLayoutAvailable.TabIndex = 6;
            this.tableLayoutAvailable.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(4, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название курса";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(333, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Краткое описание";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(678, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Начало курса";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(780, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Действие";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 461);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.btnNewCourse);
            this.Controls.Add(this.tableLayoutManageCourses);
            this.Controls.Add(this.tableLayoutStartedCourses);
            this.Controls.Add(this.optManageCourses);
            this.Controls.Add(this.tableLayoutAvailable);
            this.Controls.Add(this.btnMyProfile);
            this.Controls.Add(this.optStartedCourses);
            this.Controls.Add(this.optAvailableCourses);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(570, 350);
            this.Name = "frmMain";
            this.Text = "E-courses";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.tableLayoutManageCourses.ResumeLayout(false);
            this.tableLayoutManageCourses.PerformLayout();
            this.tableLayoutStartedCourses.ResumeLayout(false);
            this.tableLayoutStartedCourses.PerformLayout();
            this.tableLayoutAvailable.ResumeLayout(false);
            this.tableLayoutAvailable.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton optAvailableCourses;
        private System.Windows.Forms.RadioButton optStartedCourses;
        private System.Windows.Forms.Button btnMyProfile;
        private BufferedTableLayout tableLayoutAvailable;
        private System.Windows.Forms.RadioButton optManageCourses;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private BufferedTableLayout tableLayoutStartedCourses;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private BufferedTableLayout tableLayoutManageCourses;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnNewCourse;
        private System.Windows.Forms.Button btnLogout;
    }
}

