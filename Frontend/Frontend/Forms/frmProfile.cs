﻿using System;
using System.Windows.Forms;
using System.Web.Script.Serialization;

namespace Frontend
{
    public partial class frmProfile : Form
    {
        private IMessageView msgView = new MessageBoxView();

        public frmProfile()
        {
            InitializeComponent();
        }

        private bool CheckTextBox(TextBox txt, string descr)
        {
            if (txt.TextLength == 0)
            {
                MessageBox.Show(descr + " не может быть пустым", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        private void ClearTableLayout(BufferedTableLayout panel)
        {
            for (int lastRow = panel.RowCount - 1; lastRow > 0; --lastRow)
            {
                panel.RowStyles.RemoveAt(lastRow);
                for (int column = 0; column < panel.ColumnCount; ++column)
                {
                    var control = panel.GetControlFromPosition(column, lastRow);
                    panel.Controls.Remove(control);
                }

                panel.RowCount--;
            }
        }

        private void OpenCourse(int courseID)
        {
            frmCourseMaterials course = new frmCourseMaterials(courseID);
            course.ShowDialog();
        }

        private void frmProfile_Load(object sender, EventArgs e)
        {
            tableLayoutCourses.Width = this.ClientSize.Width;
            tableLayoutCourses.SetMaximumHeight(-tableLayoutCourses.Top + btnCancel.Top - 10);

            var response = TCPGlobalInstances.GetTCPServerQueryController().GetUserInfo();
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                var courses = new JavaScriptSerializer().Deserialize<Entities.UserInfo>(response.textQuery);

                txtName.Text = courses.Name;
                txtSurname.Text = courses.Surname;

                tableLayoutCourses.SuspendLayout();

                ClearTableLayout(tableLayoutCourses);
                foreach (var course in courses.EndedCourses)
                {
                    int prevRowCount = tableLayoutCourses.RowCount;
                    tableLayoutCourses.RowCount++;
                    tableLayoutCourses.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                    tableLayoutCourses.Controls.Add(new TableLayoutLabel(course.Name), 0, prevRowCount);
                    tableLayoutCourses.Controls.Add(new TableLayoutLabel(course.Description), 1, prevRowCount);
                    var btn = new TableLayoutButton(course.Id, TableLayoutButton.ButtonStyle.EnterCourse);
                    btn.Click += (senderObj, eventArgs) => OpenCourse(course.Id);
                    tableLayoutCourses.Controls.Add(btn, 2, prevRowCount);
                }

                tableLayoutCourses.ResumeLayout();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!CheckTextBox(txtName, "Поле \"Имя\"") || !CheckTextBox(txtSurname, "Поле \"Фамилия\""))
            {
                return;
            }

            var response = TCPGlobalInstances.GetTCPServerQueryController().UpdateUserInfo(txtName.Text, txtSurname.Text);
            if (response.error.Length > 0)
            {
                msgView.ShowErrorMessage(response.error);
            }
            else
            {
                msgView.ShowInfoMessage("Данные пользователя успешно изменены!");
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
