﻿using System.Windows.Forms;

namespace Frontend
{
    class TableLayoutLabel : Label
    {
        public TableLayoutLabel(string text)
            :base()
        {
            this.Text = text;
            this.Anchor = AnchorStyles.None;
            this.AutoSize = true;
        }
    }
}
