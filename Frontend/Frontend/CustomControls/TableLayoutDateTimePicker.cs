﻿using System;
using System.Windows.Forms;

namespace Frontend
{
    class TableLayoutDateTimePicker : DateTimePicker
    {
        public TableLayoutDateTimePicker(DateTime date)
            :base()
        {
            this.Value = date;
            this.Anchor = AnchorStyles.None;
            this.Dock = DockStyle.Fill;
        }
    }
}
