﻿using System.Windows.Forms;

namespace Frontend
{
    class TableLayoutTextBox : TextBox
    {
        public TableLayoutTextBox(string text)
            :base()
        {
            this.Text = text;
            this.Anchor = AnchorStyles.None;
            this.Dock = DockStyle.Fill;
            this.Multiline = true;
            this.ScrollBars = ScrollBars.Vertical;
        }
    }
}
