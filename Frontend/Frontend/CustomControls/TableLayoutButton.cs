﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Frontend
{
    class TableLayoutButton: Button
    {
        public enum ButtonStyle
        {
            NotEnrolled,
            AlreadyEnrolled,
            EnterCourse,
            DeleteMaterial,
            RowUp,
            RowDown
        }
        
        private struct StyleAttribs
        {
            public Color color;
            public string text;

            public StyleAttribs(Color clr, string newText)
            {
                color = clr;
                text = newText;
            }
        };

        private readonly Dictionary<ButtonStyle, StyleAttribs> styles = new Dictionary<ButtonStyle, StyleAttribs>
        {
            { ButtonStyle.NotEnrolled, new StyleAttribs(Color.LimeGreen, "Записаться на курс")},
            { ButtonStyle.AlreadyEnrolled, new StyleAttribs(Color.PaleVioletRed, "Отписаться с курса")},
            { ButtonStyle.EnterCourse, new StyleAttribs(Color.LightBlue, "Войти в курс")},
            { ButtonStyle.DeleteMaterial, new StyleAttribs(Color.IndianRed, "Удалить материал")},
            { ButtonStyle.RowUp, new StyleAttribs(Color.Empty, "Вверх")},
            { ButtonStyle.RowDown, new StyleAttribs(Color.Empty, "Вниз")}
        };

        private int mID;


        public TableLayoutButton(int id, ButtonStyle style)
            :base()
        {
            mID = id;
            this.AutoSize = true;
            //this.Anchor = AnchorStyles.None;

            var curStyle = styles[style];
            this.Text = curStyle.text;
            this.BackColor = curStyle.color;
        }

        public int GetID()
        {
            return mID;
        }
    }
}
