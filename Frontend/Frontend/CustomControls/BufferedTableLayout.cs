﻿using System.Windows.Forms;
using System.Drawing;

namespace Frontend
{
    class BufferedTableLayout : TableLayoutPanel
    {
        public BufferedTableLayout()
            :base()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint, true);
        }

        public void SetMaximumHeight(int height)
        {
            const int MAX_WIDTH = 1000000;

            this.MaximumSize = new Size(MAX_WIDTH, height);
            this.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
        }
    }
}
