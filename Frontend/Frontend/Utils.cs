﻿namespace Frontend
{
    static class Utils
    {
        private const string DELIM_REQUEST = "\0?\0";
        private const string EOF_REQUEST = "\x0002";
        private const string ERR_STARTER = "!!!";

        static public string CreateRequest(string command, params string[] args)
        {
            string result = command;
            if (args.Length > 0)
            {
                result += " ";
            }

            for (int i = 0; i < args.Length; ++i)
            {
                result += args[i];
                if (i < args.Length - 1)
                {
                    result += DELIM_REQUEST;
                }
            }
            result += EOF_REQUEST;

            return result;
        }

        static public string GetErrorFromResponse(string response)
        {
            if (response.Length < ERR_STARTER.Length) return "";

            string errPointer = response.Substring(0, ERR_STARTER.Length);
            if (!errPointer.Equals(ERR_STARTER)) return "";

            string err = response.Substring(ERR_STARTER.Length);
            return err;
        }
    }
}
