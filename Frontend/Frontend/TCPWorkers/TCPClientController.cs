﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Frontend
{
    class Response
    {
        public string textResponse;
        public string error;

        private Response() { }

        public Response(string response, string err)
        {
            textResponse = response;
            error = err;
        }
    }

    class TCPClientController
    {
        private const int MAX_BUFFER_RESPONSE_SIZE = 1 << 20;
        private const int MAX_RECEIVE_TIMEOUT = 5 * 1000;

        private byte[] mResponse;
        private string mIPHostString;
        private int mPort;

        private IPHostEntry mIPHost;
        private IPAddress mIPAddr;
        private IPEndPoint mIPEndpoint;

        public TCPClientController(string ipHostOrAddress, int port)
        {
            mResponse = new byte[MAX_BUFFER_RESPONSE_SIZE];
            mIPHostString = ipHostOrAddress;
            mPort = port;

            mIPHost = Dns.GetHostEntry(mIPHostString);
            mIPAddr = mIPHost.AddressList[0];
            mIPEndpoint = new IPEndPoint(mIPAddr, mPort);
        }

        public Response GetServerResponse(string request)
        {
            Socket socket = new Socket(mIPAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            string result = "";
            string error = "";

            try
            {
                socket.Connect(mIPEndpoint);

                byte[] reqByte = Encoding.UTF8.GetBytes(request);

                int bytesSent = socket.Send(reqByte);
                if (bytesSent != reqByte.Length)
                {
                    error = "Не все сообщение было отослано серверу";
                }
                else
                {
                    socket.ReceiveTimeout = MAX_RECEIVE_TIMEOUT;
                    int bytesReceived = socket.Receive(mResponse);

                    result = Encoding.UTF8.GetString(mResponse, 0, bytesReceived);
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            try
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }


            return new Response(result, error);
        } 
    }
}
