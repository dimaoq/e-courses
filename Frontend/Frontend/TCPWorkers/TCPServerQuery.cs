﻿using System;

namespace Frontend
{
    class QueryResult
    {
        public string textQuery;
        public string error;

        private QueryResult() { }

        public QueryResult(string result, string err)
        {
            textQuery = result;
            error = err;
        }
    }

    class TCPServerQuery
    {
        private string mSession;

        public TCPServerQuery(string session)
        {
            mSession = session;
        }

        static public QueryResult GetSimpleResponse(string query, string session, params string[] args)
        {
            TCPClientController ctrl = TCPGlobalInstances.GetTCPClientController();

            string[] newRequest = new string[args.Length + 1];
            newRequest[0] = session;
            for (int i = 0; i < args.Length; ++i)
            {
                newRequest[i + 1] = args[i];
            }
            var request = Utils.CreateRequest(query, newRequest);
            var result = ctrl.GetServerResponse(request);

            if (result.error.Length == 0)
            {
                string serverError = Utils.GetErrorFromResponse(result.textResponse);
                if (serverError.Length > 0)
                {
                    return new QueryResult("", serverError);
                }

                return new QueryResult(result.textResponse, "");
            }
            else
            {
                return new QueryResult("", result.error);
            }
        }

        private QueryResult queryToServer(string query, params string[] args)
        {
            return GetSimpleResponse(query, mSession, args);
        }

        public QueryResult GetAvailableCourses()
        {
            return queryToServer("getCourses");
        }

        public QueryResult GetEnrolledCourses()
        {
            return queryToServer("getEnrolledCourses");
        }

        public QueryResult GetManagedCourses()
        {
            return queryToServer("getOwnCourses");
        }

        public QueryResult AddNewCourse(string name, string description, string releaseDate, int numberOfMaterials)
        {
            return queryToServer("createCourse", name, description, releaseDate, numberOfMaterials.ToString());
        }

        public QueryResult DoEnroll(int courseID)
        {
            return queryToServer("enroll", courseID.ToString());
        }

        public QueryResult DoUnenroll(int courseID)
        {
            return queryToServer("unenroll", courseID.ToString());
        }

        public QueryResult GetCourseDetails(int courseID)
        {
            return queryToServer("courseDetails", courseID.ToString());
        }

        public QueryResult GetUserInfo()
        {
            return queryToServer("getUserInfo");
        }

        public QueryResult UpdateUserInfo(string name, string surname)
        {
            return queryToServer("updateUserInfo", name, surname);
        }

        public QueryResult GetFullCourseInfo(int courseID)
        {
            return queryToServer("getFullCourseInfo", courseID.ToString());
        }

        public QueryResult EndCourse(int courseID)
        {
            return queryToServer("endCourse", courseID.ToString());
        }

        public QueryResult UpdateCourseTextInfo(int courseID, string courseName, string description)
        {
            return queryToServer("updateCourseTextInfo", courseID.ToString(), courseName, description);
        }

        public QueryResult GetMaterial(int materialID)
        {
            return queryToServer("getMaterial", materialID.ToString());
        }

        public QueryResult UpdateMaterial(int materialID, string releaseDate, string description, string video)
        {
            return queryToServer("updateMaterial", materialID.ToString(), releaseDate, description, video);
        }

        public QueryResult CreateMaterial(int courseID, string releaseDate, string description, string video)
        {
            return queryToServer("createMaterial", courseID.ToString(), releaseDate, description, video);
        }

        public QueryResult UpdateMaterialsOrder(params int[] ids)
        {
            return queryToServer("updateMaterialsOrder", Array.ConvertAll(ids, x => x.ToString()));
        }

        public QueryResult DeleteMaterial(int materialID)
        {
            return queryToServer("deleteMaterial", materialID.ToString());
        }
    }
}
