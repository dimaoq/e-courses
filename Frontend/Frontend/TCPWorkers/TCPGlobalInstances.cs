﻿namespace Frontend
{
    static class TCPGlobalInstances
    {
        static private TCPClientController mClientControllerInstance = null;
        static private TCPServerQuery mServerQueryControllerInstance = null;

        static public void InitTCPClientController(string hostName, int port)
        {
            mClientControllerInstance = new TCPClientController(hostName, port);
        }

        static public void InitTCPServerQuery(string session)
        {
            mServerQueryControllerInstance = new TCPServerQuery(session);
        }

        static public TCPClientController GetTCPClientController()
        {
            return mClientControllerInstance;
        }

        static public TCPServerQuery GetTCPServerQueryController()
        {
            return mServerQueryControllerInstance;
        }
    }
}
