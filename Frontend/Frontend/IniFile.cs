﻿using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace Frontend
{
    class IniFile
    {
        private FileInfo mPath;
        
        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        private static extern long WritePrivateProfileString(string section, string key, 
                                                             string val, string filePath);

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        private static extern int GetPrivateProfileString(string section, string key, 
                                                          string def, StringBuilder retVal,
                                                          int size, string filePath);

        public IniFile(string path)
        {
            mPath = new FileInfo(path);
        }

        public bool Exists()
        {
            return mPath.Exists;
        }

        public string ReadValue(string section, string key)
        {
            const int MAX_VALUE_LENGTH = 255;
            StringBuilder result = new StringBuilder(MAX_VALUE_LENGTH);
            int i = GetPrivateProfileString(section, key, "", result, MAX_VALUE_LENGTH, mPath.FullName);
            return result.ToString();
        }
    }
}
