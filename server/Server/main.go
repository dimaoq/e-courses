package main

import (
	"bitbucket.org/dimaoq/e-courses/server/db"
	"bitbucket.org/dimaoq/e-courses/server/functionality"
	"bitbucket.org/dimaoq/e-courses/server/log"
	"bitbucket.org/dimaoq/e-courses/server/router"
	"bufio"
	"errors"
	"github.com/go-ini/ini"
	"net"
)

var host_port string
var user_name string
var user_password string
var db_courses_name string

var logger log.Logger = *(log.GetLogger())

func validate(s string) bool {
	return len(s) > 0
}

func loadConfiguration() error {
	const FILE_NAME = "config.ini"
	const SERVER_SETTINGS_SECTION = "server"
	const DB_SETTINGS_SECTION = "db"

	cfg, err := ini.InsensitiveLoad(FILE_NAME)
	if err != nil {
		return err
	}

	port := cfg.Section(SERVER_SETTINGS_SECTION).Key("port").String()
	userName := cfg.Section(DB_SETTINGS_SECTION).Key("userName").String()
	userPassword := cfg.Section(DB_SETTINGS_SECTION).Key("userPassword").String()
	dbName := cfg.Section(DB_SETTINGS_SECTION).Key("DBName").String()

	if validate(port) && validate(userName) && validate(userPassword) && validate(dbName) {
		host_port = port
		user_name = userName
		user_password = userPassword
		db_courses_name = dbName

		return nil
	}

	return errors.New("Not all setings have been loaded")
}

func createConnectionToDB() (*db.DBWorker, error) {
	dbWorker, err := db.CreateDBWorker(user_name, user_password, db_courses_name)
	if err != nil {
		return nil, err
	}

	return dbWorker, nil
}

func createListener() (net.Listener, error) {
	listener, err := net.Listen("tcp", ":"+host_port)
	if err != nil {
		return nil, err
	}

	return listener, nil
}

func initRouterFunctions(router *router.Router, functions *functionality.Functions) {
	router.AddRoute("getSession", functions.GetSession)
	router.AddRoute("createUser", functions.CreateUser)
	router.AddRoute("createCourse", functions.CreateNewCourse)
	router.AddRoute("getCourses", functions.GetAvailableCourses)
	router.AddRoute("getEnrolledCourses", functions.GetEnrolledCourses)
	router.AddRoute("getOwnCourses", functions.GetOwnCourses)
	router.AddRoute("enroll", functions.EnrollCourse)
	router.AddRoute("unenroll", functions.UnenrollCourse)
	router.AddRoute("courseDetails", functions.GetCourseDetails)
	router.AddRoute("getUserInfo", functions.GetUserInfo)
	router.AddRoute("updateUserInfo", functions.UpdateUserInfo)
	router.AddRoute("getFullCourseInfo", functions.GetFullCourseInfo)
	router.AddRoute("endCourse", functions.EndCourse)
	router.AddRoute("updateCourseTextInfo", functions.UpdateCourseTextInfo)
	router.AddRoute("getMaterial", functions.GetMaterial)
	router.AddRoute("createMaterial", functions.CreateMaterial)
	router.AddRoute("updateMaterialsOrder", functions.UpdateMaterialsOrder)
	router.AddRoute("updateMaterial", functions.UpdateMaterial)
	router.AddRoute("deleteMaterial", functions.DeleteMaterial)
}

func prepareToListen(dbWorker *db.DBWorker) (net.Listener, *router.Router, error) {
	listener, err := createListener()
	if err != nil {
		return nil, nil, err
	}

	functions := functionality.CreateFunctionsHandler(dbWorker)
	router := router.CreateRouter(functions.GetUserID)
	initRouterFunctions(router, functions)

	return listener, router, nil
}

func startListen(listener net.Listener, router *router.Router) {
	const EOF_listen_char = '\x02'

	for {
		conn, err := listener.Accept()
		if err != nil {
			logger.LogError("Accept listener error: " + err.Error())
			continue
		}

		request, err := bufio.NewReader(conn).ReadString(EOF_listen_char)
		if err != nil {
			logger.LogError("Incorrect request " + request + ": " + err.Error())
			continue
		}

		request = request[:len(request)-1]
		//logger.Log(request);
		if request == "shutdown" {
			break
		}

		go router.HandleRequest(conn, request)
	}
}

func main() {
	logger.LogSystem("Server starts working...")
	defer logger.Close()

	err := loadConfiguration()
	if err != nil {
		logger.LogError("Loading configuration file failed: " + err.Error())
		return
	}

	dbWorker, err := createConnectionToDB()
	if err != nil {
		logger.LogError("Connection to DB failed: " + err.Error())
		return
	}
	defer dbWorker.CloseConnection()
	logger.LogSystem("Connection to DB established!")

	listener, router, err := prepareToListen(dbWorker)
	if err != nil {
		logger.LogError("Error on preparing to listen: " + err.Error())
		return
	}
	defer listener.Close()
	logger.LogSystem("Listening on port " + host_port + " created!")

	startListen(listener, router)
}
