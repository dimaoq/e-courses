package db

import (
	"bitbucket.org/dimaoq/e-courses/server/entities"
	"database/sql"
	"errors"
	"math/rand"
	"strconv"
	"time"
)

type DBWorker struct {
	conn *DBConnector
}

func CreateDBWorker(userName, userPassword, dbName string) (*DBWorker, error) {
	connector, err := CreateConnector(userName, userPassword, dbName)
	if err != nil {
		return nil, err
	}

	if !initializeTables(connector) {
		connector.CloseConnection()
		return nil, errors.New("Initialization error!")
	}

	//init random
	rand.Seed(time.Now().UnixNano())
	return &DBWorker{conn: connector}, nil
}

func initializeTables(conn *DBConnector) bool {
	return conn.InitTable(courses_table_name, tablesSchemas[courses_table_name]) &&
		conn.InitTable(materials_table_name, tablesSchemas[materials_table_name]) &&
		conn.InitTable(tests_table_name, tablesSchemas[tests_table_name]) &&
		conn.InitTable(questions_table_name, tablesSchemas[questions_table_name]) &&
		conn.InitTable(answers_table_name, tablesSchemas[answers_table_name]) &&
		conn.InitTable(users_table_name, tablesSchemas[users_table_name]) &&
		conn.InitTable(enrolled_courses_table_name, tablesSchemas[enrolled_courses_table_name]) &&
		conn.InitTable(ended_courses_table_name, tablesSchemas[ended_courses_table_name]) &&
		conn.InitTable(passed_tests_table_name, tablesSchemas[passed_tests_table_name])
}

func (worker *DBWorker) CloseConnection() {
	worker.conn.CloseConnection()
}

func (worker *DBWorker) getDBConnection() *sql.DB {
	return worker.conn.GetConnection()
}

func (worker *DBWorker) GetUserID(sessionID string) (string, bool) {
	const selectUser = "SELECT id FROM " + users_table_name + " WHERE session = ?"
	row, err := worker.getDBConnection().Query(selectUser, sessionID)

	if err != nil {
		logger.LogError("Select in getUserID failed: " + err.Error())
		return "", false
	}
	defer row.Close()

	if !row.Next() {
		return "", false
	}

	var userID string
	err = row.Scan(&userID)
	if err != nil {
		logger.LogError("Scan error in getUserID: " + err.Error())
		return "", false
	}

	return userID, true
}

func (worker *DBWorker) GetSession(login, hashPass string) (string, bool) {
	const selectSession = "SELECT session FROM " + users_table_name + " WHERE login = ? AND hash_pass = ?"
	var session string
	err := worker.getDBConnection().QueryRow(selectSession, login, hashPass).Scan(&session)

	if err != nil {
		return "", false
	}

	return session, true
}

func (worker *DBWorker) randStringBytes(n int) string {
	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func (worker *DBWorker) CreateUser(login string, hashPass string, name string, surname string) bool {
	const createUsers = "INSERT INTO " + users_table_name + " (login, hash_pass, lastLoginTime, name, surname, session, expireTime, isExpired) VALUES (?, ?, ?, ?, ?, ?, ?, false)"
	dt := time.Now()
	_, err := worker.getDBConnection().Exec(createUsers, login, hashPass, dt, name, surname, worker.randStringBytes(64), dt)

	if err != nil {
		logger.LogError("CreateUser create error: " + err.Error())
		return false
	}

	return true
}

func isArrayElement(arr *[]int, val int) bool {
	for _, elem := range *arr {
		if elem == val {
			return true
		}
	}

	return false
}

func (worker *DBWorker) CreateMaterial(courseID string, releaseDate string, description string, video string) (int64, bool) {
	const createMaterial = "INSERT INTO " + materials_table_name + " (idCourse, releaseDate, description, video) VALUES (?, ?, ?, ?)"
	res, err := worker.getDBConnection().Exec(createMaterial, courseID, releaseDate, description, video)

	if err != nil {
		logger.LogError("CreateMaterial create error: " + err.Error())
		return 0, false
	}

	newMaterialID, _ := res.LastInsertId()
	return newMaterialID, true
}

func (worker *DBWorker) CreateNewCourse(userID, name, description, releaseDate, numberOfMaterials string) bool {
	const createCourse = "INSERT INTO " + courses_table_name + "(name, authorID, description, releaseDate, numberOfMaterials, isEnded) VALUES (?, ?, ?, ?, ?, false)"
	const updateMaterial = "UPDATE " + materials_table_name + " SET idPrevMaterial = ? WHERE id = ?"

	tx, err := worker.getDBConnection().Begin()
	if err != nil {
		logger.LogError("CreateNewCourse, failed to start transaction: " + err.Error())
		return false
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}

		err = tx.Commit()
		if err != nil {
			logger.LogError("Can't commit transaction: " + err.Error())
		}
	}()

	var courseInsertResult sql.Result
	if courseInsertResult, err = tx.Exec(createCourse, name, userID, description, releaseDate, numberOfMaterials); err != nil {
		logger.LogError("CreateNewCourse, failed to create course: " + err.Error())
		return false
	}

	courseID, _ := courseInsertResult.LastInsertId()
	countOfMaterials, _ := strconv.Atoi(numberOfMaterials)

	var lastRowID int64
	for i := 0; i < countOfMaterials; i++ {
		rowID, isOk := worker.CreateMaterial(strconv.FormatInt(courseID, 10), releaseDate, "", "")
		if !isOk {
			return false
		}

		if i > 0 {
			_, err = tx.Exec(updateMaterial, lastRowID, rowID)
			if err != nil {
				logger.LogError("CreateNewCourse, failed to create material: " + err.Error())
				return false
			}
		}

		lastRowID = rowID
	}

	return true
}

func (worker *DBWorker) getEnrolledCoursesByUser(userID string) ([]int, bool) {
	const selectEnrolledCourses = "SELECT idCourse FROM " + enrolled_courses_table_name + " WHERE idUser = ?"
	rows, err := worker.getDBConnection().Query(selectEnrolledCourses, userID)

	if err != nil {
		logger.LogError("getEnrolledCoursesByUser error: " + err.Error())
		return nil, false
	}
	defer rows.Close()

	result := make([]int, 0)
	for rows.Next() {
		var num int

		err = rows.Scan(&num)
		if err != nil {
			logger.LogError("getEnrolledCoursesByUser error in scanning: " + err.Error())
			return nil, false
		}

		result = append(result, num)
	}

	return result, true
}

func (worker *DBWorker) getEndedCoursesByUser(userID string) ([]int, bool) {
	const selectEndedCourses = "SELECT idCourse FROM " + ended_courses_table_name + " WHERE idUser = ?"
	rows, err := worker.getDBConnection().Query(selectEndedCourses, userID)

	if err != nil {
		logger.LogError("getEndedCoursesByUser error: " + err.Error())
		return nil, false
	}
	defer rows.Close()

	result := make([]int, 0)
	for rows.Next() {
		var num int

		err = rows.Scan(&num)
		if err != nil {
			logger.LogError("getEndedCoursesByUser error in scanning: " + err.Error())
			return nil, false
		}

		result = append(result, num)
	}

	return result, true
}

func (worker *DBWorker) getMaterialsByCourse(courseID string) ([]entities.Material, bool) {
	const selectMaterials = "SELECT id, idPrevMaterial, releaseDate, description, video FROM " + materials_table_name + " WHERE idCourse = ?"
	rows, err := worker.getDBConnection().Query(selectMaterials, courseID)

	if err != nil {
		logger.LogError("getMaterialsByCourse error: " + err.Error())
		return nil, false
	}
	defer rows.Close()

	result := make([]entities.Material, 0)
	prevMaterials := make([]sql.NullInt64, 0)
	for rows.Next() {
		var (
			id             int64
			idPrevMaterial sql.NullInt64
			releaseDate    string
			description    string
			video          string
		)

		err = rows.Scan(&id, &idPrevMaterial, &releaseDate, &description, &video)
		if err != nil {
			logger.LogError("getMaterialsByCourse error in scanning: " + err.Error())
			return nil, false
		}

		prevMaterials = append(prevMaterials, idPrevMaterial)
		result = append(result, entities.Material{Id: id, ReleaseDate: releaseDate, Description: description, Video: video})
	}

	materials := make([]entities.Material, len(result))
	var curID int64 = -1
	for i := 0; i < len(result); i++ {
		for j, prevResult := range prevMaterials {
			if (i == 0 && !prevResult.Valid) || (i > 0 && prevResult.Valid && prevResult.Int64 == curID) {
				materials[i] = result[j]
				curID = result[j].Id
				break
			}
		}
	}

	return materials, true
}

func (worker *DBWorker) GetAvailableCourses(userID string) []entities.ShortCourseDescription {
	const selectAvailable = "SELECT id, name, description, releaseDate FROM " + courses_table_name + " WHERE (isEnded = false) AND (authorID <> ?)"
	rows, err := worker.getDBConnection().Query(selectAvailable, userID)

	if err != nil {
		logger.LogError("GetAvailableCourses select error: " + err.Error())
		return nil
	}
	defer rows.Close()

	enrolledCourses, isOk := worker.getEnrolledCoursesByUser(userID)
	if !isOk {
		return nil
	}

	result := make([]entities.ShortCourseDescription, 0)
	for rows.Next() {
		var (
			id          int
			name        string
			description string
			releaseDate string
		)

		err := rows.Scan(&id, &name, &description, &releaseDate)
		if err != nil {
			logger.LogError("GetAvailableCourses scan error: " + err.Error())
			return nil
		}
		result = append(result, entities.ShortCourseDescription{Id: id, Name: name, Description: description, ReleaseDate: releaseDate, IsEnrolledByUser: isArrayElement(&enrolledCourses, id)})
	}

	return result
}

func (worker *DBWorker) GetEnrolledCourses(userID string) []entities.ShortEnrolledCourseDescription {
	courseIDs, isOk := worker.getEnrolledCoursesByUser(userID)
	if !isOk {
		return nil
	}

	result := make([]entities.ShortEnrolledCourseDescription, len(courseIDs))
	const selectCourse = "SELECT name, releaseDate FROM " + courses_table_name + " WHERE id = ?"
	for i, courseID := range courseIDs {
		var name, releaseDate string
		err := worker.getDBConnection().QueryRow(selectCourse, courseID).Scan(&name, &releaseDate)
		if err != nil {
			logger.LogError("GetEnrolledCourses scan error: " + err.Error())
			return nil
		}

		result[i] = entities.ShortEnrolledCourseDescription{Id: courseID, Name: name, ReleaseDate: releaseDate}
	}

	return result
}

func (worker *DBWorker) GetOwnCourses(userID string) []entities.ShortOwnCourseDescription {
	const selectOwnCourses = "SELECT id, name FROM " + courses_table_name + " WHERE authorID = ?"
	rows, err := worker.getDBConnection().Query(selectOwnCourses, userID)

	if err != nil {
		logger.LogError("GetOwnCourses select error: " + err.Error())
		return nil
	}
	defer rows.Close()

	result := make([]entities.ShortOwnCourseDescription, 0)
	for rows.Next() {
		var (
			id   int
			name string
		)

		err := rows.Scan(&id, &name)
		if err != nil {
			logger.LogError("GetOwnCourses scan error: " + err.Error())
			return nil
		}
		result = append(result, entities.ShortOwnCourseDescription{Id: id, Name: name})
	}

	return result
}

func (worker *DBWorker) EnrollCourse(userID string, courseID string) bool {
	const insertEnrollCmd = "INSERT INTO " + enrolled_courses_table_name + "(idUser, idCourse) VALUES (?, ?)"
	_, err := worker.getDBConnection().Query(insertEnrollCmd, userID, courseID)

	if err != nil {
		logger.LogError("EnrollCourse insert error: " + err.Error())
		return false
	}

	return true
}

func (worker *DBWorker) UnenrollCourse(userID string, courseID string) bool {
	const deleteUnenrollCmd = "DELETE FROM " + enrolled_courses_table_name + " WHERE idUser = ? AND idCourse = ?"
	_, err := worker.getDBConnection().Query(deleteUnenrollCmd, userID, courseID)

	if err != nil {
		logger.LogError("UnenrollCourse delete error: " + err.Error())
		return false
	}

	return true
}

func (worker *DBWorker) GetCourseDetails(courseID string) *entities.CourseDetails {
	const selectCourse = "SELECT name, authorID, releaseDate, numberOfMaterials, description FROM " + courses_table_name + " WHERE id = ?"
	var (
		name              string
		authorID          int
		releaseDate       string
		numberOfMaterials int
		description       string
	)
	err := worker.getDBConnection().QueryRow(selectCourse, courseID).Scan(&name, &authorID, &releaseDate, &numberOfMaterials, &description)

	if err != nil {
		logger.LogError("GetCourseDetails select from courses table error: " + err.Error())
		return nil
	}

	const selectUser = "SELECT name, surname FROM " + users_table_name + " WHERE id = ?"
	var (
		userName    string
		userSurname string
	)
	err = worker.getDBConnection().QueryRow(selectUser, authorID).Scan(&userName, &userSurname)

	if err != nil {
		logger.LogError("GetCourseDetails select from users table error: " + err.Error())
		return nil
	}

	return &entities.CourseDetails{Name: name, Author: userName + " " + userSurname, ReleaseDate: releaseDate, NumberOfMaterials: numberOfMaterials,
		Description: description}
}

func (worker *DBWorker) GetUserInfo(userID string) *entities.FullUserInfo {
	const selectUser = "SELECT name, surname FROM " + users_table_name + " WHERE id = ?"
	var (
		userName    string
		userSurname string
	)
	err := worker.getDBConnection().QueryRow(selectUser, userID).Scan(&userName, &userSurname)
	if err != nil {
		logger.LogError("GetUserInfo select from users table error: " + err.Error())
		return nil
	}

	courseIDs, isOk := worker.getEndedCoursesByUser(userID)
	if !isOk {
		return nil
	}

	endedCourses := make([]entities.EndedCourseDescription, len(courseIDs))
	const selectCourse = "SELECT name, description FROM " + courses_table_name + " WHERE id = ?"
	for i, courseID := range courseIDs {
		var (
			name        string
			description string
		)
		err := worker.getDBConnection().QueryRow(selectCourse, courseID).Scan(&name, &description)
		if err != nil {
			logger.LogError("GetUserInfo scan error: " + err.Error())
			return nil
		}

		endedCourses[i] = entities.EndedCourseDescription{Id: courseID, Name: name, Description: description}
	}

	return &entities.FullUserInfo{Name: userName, Surname: userSurname, EndedCourses: endedCourses}
}

func (worker *DBWorker) UpdateUserInfo(userID string, name string, surname string) bool {
	const updateUserInfoCmd = "UPDATE " + users_table_name + " SET name = ?, surname = ? WHERE id = ?"
	_, err := worker.getDBConnection().Exec(updateUserInfoCmd, name, surname, userID)

	if err != nil {
		logger.LogError("UpdateUserInfo update error: " + err.Error())
		return false
	}

	return true
}

func (worker *DBWorker) GetFullCourseInfo(courseID string) *entities.FullCourseInfo {
	const selectCourse = "SELECT name, description, isEnded FROM " + courses_table_name + " WHERE id = ?"
	var (
		name        string
		description string
		isEnded		bool
	)
	err := worker.getDBConnection().QueryRow(selectCourse, courseID).Scan(&name, &description, &isEnded)
	if err != nil {
		logger.LogError("GetFullCourseInfo select from courses table error: " + err.Error())
		return nil
	}

	materials, isOk := worker.getMaterialsByCourse(courseID)
	if !isOk {
		return nil
	}

	return &entities.FullCourseInfo{Name: name, Description: description, Materials: materials, IsEnded: isEnded}
}

func (worker *DBWorker) EndCourse(courseID string) bool {
	const selectUsers = "SELECT idUser FROM " + enrolled_courses_table_name + " WHERE idCourse = ?"
	rows, err := worker.getDBConnection().Query(selectUsers, courseID)

	if err != nil {
		logger.LogError("EndCourse select error: " + err.Error())
		return false
	}
	defer rows.Close()
	
	tx, err := worker.getDBConnection().Begin()
	if err != nil {
		logger.LogError("EndCourse, failed to start transaction: " + err.Error())
		return false
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}

		err = tx.Commit()
		if err != nil {
			logger.LogError("Can't commit transaction: " + err.Error())
		}
	}()
	
	const updateEndCourse = "UPDATE " + courses_table_name + " SET isEnded = true WHERE id = ?"
	_, err = worker.getDBConnection().Exec(updateEndCourse, courseID)

	if err != nil {
		logger.LogError("EndCourse update error: " + err.Error())
		return false
	}
	
	const insertEndedCourse = "INSERT INTO " + ended_courses_table_name + " (idUser, idCourse) VALUES (?, ?)"
	for rows.Next() {
		var idUser int
		err = rows.Scan(&idUser)
		
		if err != nil {
			logger.LogError("EndCourse scan error: " + err.Error())
			return false
		}
		
		_, err = worker.getDBConnection().Query(insertEndedCourse, idUser, courseID)

		if err != nil {
			logger.LogError("EndCourse insert error: " + err.Error())
			return false
		}
	}
	
	return true
}

func (worker *DBWorker) UpdateCourseTextInfo(courseID string, name string, description string) bool {
	const updateCourseInfoCmd = "UPDATE " + courses_table_name + " SET name = ?, description = ? WHERE id = ?"
	_, err := worker.getDBConnection().Exec(updateCourseInfoCmd, name, description, courseID)

	if err != nil {
		logger.LogError("UpdateCourseTextInfo update error: " + err.Error())
		return false
	}

	return true
}

func (worker *DBWorker) GetMaterial(materialID string) *entities.Material {
	const selectMaterial = "SELECT id, releaseDate, description, video FROM " + materials_table_name + " WHERE id = ?"
	var (
		id          int64
		releaseDate string
		description string
		video       string
	)
	err := worker.getDBConnection().QueryRow(selectMaterial, materialID).Scan(&id, &releaseDate, &description, &video)
	if err != nil {
		logger.LogError("GetMaterial select from materials table error: " + err.Error())
		return nil
	}

	return &entities.Material{Id: id, ReleaseDate: releaseDate, Description: description, Video: video}
}

func (worker *DBWorker) UpdateMaterialsOrder(ids []string) bool {
	const updateMaterialOrder = "UPDATE " + materials_table_name + " SET idPrevMaterial = ? WHERE id = ?"

	tx, err := worker.getDBConnection().Begin()
	if err != nil {
		logger.LogError("UpdateMaterialsOrder, failed to start transaction: " + err.Error())
		return false
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}

		err = tx.Commit()
		if err != nil {
			logger.LogError("Can't commit transaction: " + err.Error())
		}
	}()

	for i := 1; i < len(ids); i++ {
		if i == 1 {
			_, err = tx.Exec(updateMaterialOrder, nil, ids[i])
		} else {
			_, err = tx.Exec(updateMaterialOrder, ids[i-1], ids[i])
		}

		if err != nil {
			logger.LogError("UpdateMaterialsOrder update error: " + err.Error())
			return false
		}
	}

	return true
}

func (worker *DBWorker) UpdateMaterial(materialID string, releaseDate string, description string, video string) bool {
	const updateMaterial = "UPDATE " + materials_table_name + " SET releaseDate = ?, description = ?, video = ? WHERE id = ?"
	_, err := worker.getDBConnection().Exec(updateMaterial, releaseDate, description, video, materialID)

	if err != nil {
		logger.LogError("UpdateMaterial update error: " + err.Error())
		return false
	}

	return true
}

func (worker *DBWorker) DeleteMaterial(materialID string) bool {
	tx, err := worker.getDBConnection().Begin()
	if err != nil {
		logger.LogError("DeleteMaterial, failed to start transaction: " + err.Error())
		return false
	}
	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}

		err = tx.Commit()
		if err != nil {
			logger.LogError("Can't commit transaction: " + err.Error())
		}
	}()

	const selectCurPrevMaterial = "SELECT idPrevMaterial FROM " + materials_table_name + " WHERE id = ?"
	var prevID sql.NullInt64
	err = tx.QueryRow(selectCurPrevMaterial, materialID).Scan(&prevID)
	if err != nil {
		logger.LogError("DeleteMaterial select error: " + err.Error())
		return false
	}

	const selectPrevMaterial = "SELECT id FROM " + materials_table_name + " WHERE idPrevMaterial = ?"
	var idToUpdate int
	err = tx.QueryRow(selectPrevMaterial, materialID).Scan(&idToUpdate)
	if err == sql.ErrNoRows {
		idToUpdate = -1
	} else {
		if err != nil {
			logger.LogError("DeleteMaterial select error: " + err.Error())
			return false
		}
	}

	const deleteMaterial = "DELETE FROM " + materials_table_name + " WHERE id = ?"
	_, err = tx.Exec(deleteMaterial, materialID)

	if err != nil {
		logger.LogError("DeleteMaterial update error: " + err.Error())
		return false
	}

	if idToUpdate != -1 {
		const updateNewMaterial = "UPDATE " + materials_table_name + " SET idPrevMaterial = ? WHERE id = ?"
		if !prevID.Valid {
			_, err = tx.Exec(updateNewMaterial, nil, idToUpdate)
		} else {
			_, err = tx.Exec(updateNewMaterial, prevID, idToUpdate)
		}

		if err != nil {
			logger.LogError("DeleteMaterial update error: " + err.Error())
			return false
		}
	}

	return true
}
