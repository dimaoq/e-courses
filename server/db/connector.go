package db

import (
	"bitbucket.org/dimaoq/e-courses/server/log"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type DBConnector struct {
	dbConnection *sql.DB
}

var logger log.Logger = *(log.GetLogger())

func CreateConnector(userName, userPassword, dbName string) (*DBConnector, error) {
	db, err := sql.Open("mysql", userName+":"+userPassword+"@/"+dbName)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return &DBConnector{dbConnection: db}, nil
}

func (connector *DBConnector) CloseConnection() {
	if connector.dbConnection != nil {
		connector.dbConnection.Close()
		logger.LogSystem("Connection destroyed!")
	}
}

func (connector *DBConnector) GetConnection() *sql.DB {
	return connector.dbConnection
}

func (connector *DBConnector) createTable(tableName, schema string) bool {
	stmtCreateTable, err := connector.dbConnection.Prepare("CREATE TABLE IF NOT EXISTS " + tableName + " " + schema)
	if err != nil {
		logger.LogError("createTable error, table " + tableName + ": " + err.Error())
		return false
	}
	defer stmtCreateTable.Close()

	_, err = stmtCreateTable.Exec()
	if err != nil {
		logger.LogError("createTable error, table " + tableName + ": " + err.Error())
		return false
	}

	logger.Log("Table " + tableName + " initialized!")
	return true
}

func (connector *DBConnector) InitTable(tableName, tableSchema string) bool {
	return connector.createTable(tableName, tableSchema)
}
