package db

const courses_table_name string = "courses"
const materials_table_name string = "materials"
const tests_table_name string = "tests"
const questions_table_name string = "faq_questions"
const answers_table_name string = "faq_answers"
const users_table_name string = "users"
const enrolled_courses_table_name string = "enrolled_courses"
const ended_courses_table_name string = "ended_courses"
const passed_tests_table_name string = "passed_tests"

var tablesSchemas map[string]string = map[string]string{
	courses_table_name: `(id BIGINT UNSIGNED AUTO_INCREMENT,
						  authorID BIGINT UNSIGNED NOT NULL,
						  name TEXT NOT NULL,
						  description TEXT NOT NULL, 
						  releaseDate DATE NOT NULL, 
						  numberOfMaterials TINYINT UNSIGNED NOT NULL,
						  isEnded BOOLEAN NOT NULL, 
						  PRIMARY KEY (id))`,

	materials_table_name: `(id BIGINT UNSIGNED AUTO_INCREMENT,
							idCourse BIGINT UNSIGNED NOT NULL,
							idPrevMaterial BIGINT UNSIGNED NULL,
							releaseDate DATE NOT NULL, 
							description TEXT NOT NULL, 
							video TEXT NOT NULL, 
							test BIGINT UNSIGNED NULL,
							PRIMARY KEY (id))`,

	tests_table_name: `(id BIGINT UNSIGNED AUTO_INCREMENT,
						question TEXT NOT NULL,
						answers TEXT NOT NULL, correctAnswer BIGINT UNSIGNED NOT NULL,
						PRIMARY KEY (id))`,

	questions_table_name: `(id BIGINT UNSIGNED AUTO_INCREMENT,
							materialID BIGINT UNSIGNED NOT NULL,
							authorID BIGINT UNSIGNED NOT NULL,
							questionDate DATETIME NOT NULL, question TEXT NOT NULL, 
							PRIMARY KEY (id))`,

	answers_table_name: `(id BIGINT UNSIGNED AUTO_INCREMENT,
						  questionID BIGINT UNSIGNED NOT NULL,
						  authorID BIGINT UNSIGNED NOT NULL,
						  answerDate DATETIME NOT NULL, answer TEXT NOT NULL, 
						  PRIMARY KEY (id))`,

	users_table_name: `(id BIGINT UNSIGNED AUTO_INCREMENT,
						login VARCHAR(50) NOT NULL,	hash_pass CHAR(64) NOT NULL,
						lastLoginTime DATETIME NOT NULL,
						name VARCHAR(50) NOT NULL, surname VARCHAR(50) NOT NULL,
						session CHAR(64) NOT NULL, expireTime DATETIME NOT NULL, isExpired BOOLEAN NOT NULL,
						PRIMARY KEY (id), UNIQUE KEY (session))`,

	enrolled_courses_table_name: `(idUser BIGINT UNSIGNED NOT NULL,
								   idCourse BIGINT UNSIGNED NOT NULL,
								   PRIMARY KEY (idUser, idCourse))`,

	ended_courses_table_name: `(idUser BIGINT UNSIGNED NOT NULL,
								idCourse BIGINT UNSIGNED NOT NULL,
								PRIMARY KEY (idUser, idCourse))`,

	passed_tests_table_name: `(idUser BIGINT UNSIGNED NOT NULL,
							   idTest BIGINT UNSIGNED NOT NULL,
							   PRIMARY KEY (idUser, idTest))`,
}
