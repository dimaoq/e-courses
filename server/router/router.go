package router

import (
	"bitbucket.org/dimaoq/e-courses/server/log"
	"bitbucket.org/dimaoq/e-courses/server/response"
	"errors"
	"net"
	"strings"
	"sync"
)

type routeFunc func([]string) []byte
type mapRouteFunc map[string]routeFunc
type userIDGetter func(string) (string, bool)

type Router struct {
	routesMap mapRouteFunc
	mutex     *sync.Mutex
	getUserID userIDGetter
}

const splits_count int = 2
const split_request_delim string = " "
const split_arguments_delim string = "\x00?\x00"

var logger log.Logger = *(log.GetLogger())

func CreateRouter(funcGetUserID userIDGetter) *Router {
	return &Router{routesMap: make(mapRouteFunc), mutex: &sync.Mutex{}, getUserID: funcGetUserID}
}

func (router *Router) AddRoute(query string, function routeFunc) {
	router.mutex.Lock()
	router.routesMap[query] = function
	router.mutex.Unlock()
}

func (router *Router) splitRequest(request string) ([]string, error) {
	splittedRequest := strings.SplitN(request, split_request_delim, splits_count)
	if len(splittedRequest) < splits_count {
		return nil, errors.New("Split request, not enough arguments: " + request)
	}

	return splittedRequest, nil
}

func (router *Router) splitFuncArguments(args string) []string {
	return strings.Split(args, split_arguments_delim)
}

func (router *Router) getFunction(funcName string) (routeFunc, error) {
	router.mutex.Lock()
	funcToHandle, isOk := router.routesMap[funcName]
	router.mutex.Unlock()

	if !isOk {
		return nil, errors.New("Routing failed, unknown function: " + funcName)
	}

	return funcToHandle, nil
}

func (router *Router) HandleRequest(connection net.Conn, request string) {
	defer connection.Close()

	splittedRequest, err := router.splitRequest(request)
	if err != nil {
		logger.LogError(err.Error())
		connection.Write(response.CreateErrorResponse("Not enough arguments in query"))
		return
	}

	request = splittedRequest[0]
	funcArguments := splittedRequest[1]

	funcToHandle, err := router.getFunction(request)
	if err != nil {
		logger.LogError(err.Error())
		connection.Write(response.CreateErrorResponse("Unknown function call"))
		return
	}

	params := router.splitFuncArguments(funcArguments)

	if len(params[0]) > 0 {
		userSession := params[0]
		userID, isOk := router.getUserID(userSession)
		if !isOk {
			logger.Log("User with unknown session: " + userSession)
			connection.Write(response.CreateErrorResponse("User is not found"))
			return
		}
		params[0] = userID
	}

	connection.Write(funcToHandle(params))
}
