package functionality

import (
	"bitbucket.org/dimaoq/e-courses/server/db"
	"bitbucket.org/dimaoq/e-courses/server/log"
	"bitbucket.org/dimaoq/e-courses/server/response"
	"strconv"
)

type Functions struct {
	dbWorker *db.DBWorker
}

var logger log.Logger = *(log.GetLogger())

func CreateFunctionsHandler(db *db.DBWorker) *Functions {
	return &Functions{dbWorker: db}
}

func (functions *Functions) checkParams(params *[]string, correctLen int) int {
	lenParams := len(*params)

	if lenParams > correctLen {
		return 1
	}

	if lenParams < correctLen {
		return -1
	}

	return 0
}

func (functions *Functions) GetUserID(sessionID string) (string, bool) {
	return functions.dbWorker.GetUserID(sessionID)
}

func (functions *Functions) GetSession(params []string) []byte {
	paramsValid := functions.checkParams(&params, 3)
	if paramsValid != 0 {
		logger.Log("GetSession, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result, isOk := functions.dbWorker.GetSession(params[1], params[2])
	if !isOk {
		return response.CreateErrorResponse("No such user!")
	}

	return response.CreateRawResponse(result)
}

func (functions *Functions) CreateUser(params []string) []byte {
	paramsValid := functions.checkParams(&params, 5)
	if paramsValid != 0 {
		logger.Log("CreateUser, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	isCreated := functions.dbWorker.CreateUser(params[1], params[2], params[3], params[4])
	if isCreated {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("User is not created!")
	}
}

func (functions *Functions) GetAvailableCourses(params []string) []byte {
	paramsValid := functions.checkParams(&params, 1)

	if paramsValid != 0 {
		logger.Log("getAvailableCourses, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.GetAvailableCourses(params[0])
	if result == nil {
		return response.CreateErrorResponse("Available courses == NULL")
	}

	return response.CreateJSONResponse(result)
}

func (functions *Functions) CreateNewCourse(params []string) []byte {
	paramsValid := functions.checkParams(&params, 5)

	if paramsValid != 0 {
		logger.Log("createNewCourse, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	isCreated := functions.dbWorker.CreateNewCourse(params[0], params[1], params[2], params[3], params[4])
	if isCreated {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("Course is not created!")
	}
}

func (functions *Functions) GetEnrolledCourses(params []string) []byte {
	paramsValid := functions.checkParams(&params, 1)

	if paramsValid != 0 {
		logger.Log("getEnrolledCourses, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.GetEnrolledCourses(params[0])
	if result == nil {
		return response.CreateErrorResponse("Enrolled courses == NULL")
	}

	return response.CreateJSONResponse(result)
}

func (functions *Functions) GetOwnCourses(params []string) []byte {
	paramsValid := functions.checkParams(&params, 1)

	if paramsValid != 0 {
		logger.Log("getOwnCourses, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.GetOwnCourses(params[0])
	if result == nil {
		return response.CreateErrorResponse("Own courses == NULL")
	}

	return response.CreateJSONResponse(result)
}

func (functions *Functions) EnrollCourse(params []string) []byte {
	paramsValid := functions.checkParams(&params, 2)

	if paramsValid != 0 {
		logger.Log("enrollCourse, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.EnrollCourse(params[0], params[1])
	if result {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("Enroll failed")
	}
}

func (functions *Functions) UnenrollCourse(params []string) []byte {
	paramsValid := functions.checkParams(&params, 2)

	if paramsValid != 0 {
		logger.Log("unenrollCourse, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.UnenrollCourse(params[0], params[1])
	if result {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("Unenroll failed")
	}
}

func (functions *Functions) GetCourseDetails(params []string) []byte {
	paramsValid := functions.checkParams(&params, 2)

	if paramsValid != 0 {
		logger.Log("getCourseDetails, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.GetCourseDetails(params[1])
	if result == nil {
		return response.CreateErrorResponse("Courses == NULL")
	}

	return response.CreateJSONResponse(result)
}

func (functions *Functions) GetUserInfo(params []string) []byte {
	paramsValid := functions.checkParams(&params, 1)

	if paramsValid != 0 {
		logger.Log("GetUserInfo, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.GetUserInfo(params[0])
	if result == nil {
		return response.CreateErrorResponse("User == NULL")
	}

	return response.CreateJSONResponse(result)
}

func (functions *Functions) UpdateUserInfo(params []string) []byte {
	paramsValid := functions.checkParams(&params, 3)

	if paramsValid != 0 {
		logger.Log("UpdateUserInfo, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.UpdateUserInfo(params[0], params[1], params[2])
	if result {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("UpdateUserInfo failed")
	}
}

func (functions *Functions) GetFullCourseInfo(params []string) []byte {
	paramsValid := functions.checkParams(&params, 2)

	if paramsValid != 0 {
		logger.Log("GetFullCourseInfo, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.GetFullCourseInfo(params[1])
	if result == nil {
		return response.CreateErrorResponse("Course == NULL")
	}

	return response.CreateJSONResponse(result)
}

func (functions *Functions) EndCourse(params []string) []byte {
	paramsValid := functions.checkParams(&params, 2)

	if paramsValid != 0 {
		logger.Log("EndCourse, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.EndCourse(params[1])
	if result {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("EndCourse failed")
	}
}

func (functions *Functions) UpdateCourseTextInfo(params []string) []byte {
	paramsValid := functions.checkParams(&params, 4)

	if paramsValid != 0 {
		logger.Log("UpdateCourseTextInfo, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.UpdateCourseTextInfo(params[1], params[2], params[3])
	if result {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("UpdateCourseTextInfo failed")
	}
}

func (functions *Functions) GetMaterial(params []string) []byte {
	paramsValid := functions.checkParams(&params, 2)

	if paramsValid != 0 {
		logger.Log("GetMaterial, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.GetMaterial(params[1])
	if result == nil {
		return response.CreateErrorResponse("Material == NULL")
	}

	return response.CreateJSONResponse(result)
}

func (functions *Functions) CreateMaterial(params []string) []byte {
	paramsValid := functions.checkParams(&params, 5)

	if paramsValid != 0 {
		logger.Log("CreateMaterial, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result, isOk := functions.dbWorker.CreateMaterial(params[1], params[2], params[3], params[4])
	if !isOk {
		return response.CreateErrorResponse("Material == NULL")
	}

	return response.CreateRawResponse(strconv.FormatInt(result, 10))
}

func (functions *Functions) UpdateMaterialsOrder(params []string) []byte {
	result := functions.dbWorker.UpdateMaterialsOrder(params)
	if result {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("UpdateMaterialsOrder failed")
	}
}

func (functions *Functions) UpdateMaterial(params []string) []byte {
	paramsValid := functions.checkParams(&params, 5)

	if paramsValid != 0 {
		logger.Log("UpdateMaterial, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.UpdateMaterial(params[1], params[2], params[3], params[4])
	if result {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("UpdateMaterial failed")
	}
}

func (functions *Functions) DeleteMaterial(params []string) []byte {
	paramsValid := functions.checkParams(&params, 2)

	if paramsValid != 0 {
		logger.Log("DeleteMaterial, checkParams = " + strconv.Itoa(paramsValid))
		return response.CreateErrorResponse("Wrong number of arguments")
	}

	result := functions.dbWorker.DeleteMaterial(params[1])
	if result {
		return response.CreateOKResponse()
	} else {
		return response.CreateErrorResponse("DeleteMaterial failed")
	}
}
