package response

import (
	"bitbucket.org/dimaoq/e-courses/server/log"
	"encoding/json"
)

const error_starter = "!!!"

var logger log.Logger = *(log.GetLogger())

func CreateErrorResponse(strResponse string) []byte {
	return []byte(error_starter + strResponse)
}

func CreateRawResponse(strResponse string) []byte {
	return []byte(strResponse)
}

func CreateOKResponse() []byte {
	return []byte("1")
}

func CreateJSONResponse(jsonInterface interface{}) []byte {
	response, err := json.Marshal(jsonInterface)
	if err != nil {
		logger.LogError("Can't translate into JSON: " + err.Error())
		return CreateErrorResponse("Server JSON-parser error")
	}

	return response
}
