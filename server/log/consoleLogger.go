package log

import (
	"fmt"
	"sync"
)

type consoleLogger struct {
	sync.Mutex
}

func (*consoleLogger) open() bool {
	return true
}

func (logger *consoleLogger) Log(logString string) {
	logger.Lock()
	fmt.Println(logString)
	logger.Unlock()
}

func (logger *consoleLogger) LogError(logString string) {
	logger.Log("[ERROR] " + logString)
}

func (logger *consoleLogger) LogSystem(logString string) {
	logger.Log("*****" + logString + "*****")
}

func (*consoleLogger) Close() {
}
