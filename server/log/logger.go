package log

type Logger interface {
	open() bool
	Log(string)
	LogError(string)
	LogSystem(string)
	Close()
}

var curLogger Logger

func GetLogger() *Logger {
	if curLogger == nil {
		curLogger = &consoleLogger{}
		if !curLogger.open() {
			panic("CAN'T CREATE LOGGER!")
		}
	}

	return &curLogger
}
