package entities

type ShortCourseDescription struct {
	Id               int
	Name             string
	Description      string
	ReleaseDate      string
	IsEnrolledByUser bool
}

type CourseDetails struct {
	Name              string
	Author            string
	ReleaseDate       string
	NumberOfMaterials int
	Description       string
}

type ShortEnrolledCourseDescription struct {
	Id          int
	Name        string
	ReleaseDate string
}

type ShortOwnCourseDescription struct {
	Id   int
	Name string
}

type EndedCourseDescription struct {
	Id			int
	Name        string
	Description string
}

type FullUserInfo struct {
	Name         string
	Surname      string
	EndedCourses []EndedCourseDescription
}

type Material struct {
	Id          int64
	ReleaseDate string
	Description string
	Video       string
}

type FullCourseInfo struct {
	Name        string
	Description string
	Materials   []Material
	IsEnded		bool
}
